---
name: About DebConf
---
# About DebConf

DebConf is the annual conference for [Debian](http://debian.org) contributors
and users interested in [improving Debian](https://www.debian.org/intro/help).
[Previous Debian conferences](https://www.debconf.org) have featured speakers
and attendees from all around the world. The last DebConf, [DebConf22][], took
place in Prizren, Kosovo and was attended by 214 participants from over 44
countries.

[DebConf22]: https://debconf22.debconf.org/

**DebConf23 is taking place in Kochi, India from September 10 to September 17,
2023.**

It is being preceded by DebCamp, from September 03 to September 09, 2023.

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Register to the debconf-announce mailing list</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

<br />
## Venue

[Infopark, Kochi](https://infopark.in/) is an information technology park situated in the city of Kochi, Kerala, India.
Established in 2004 by the Government of Kerala. The park is spread over 260 acres (105.2 ha) of campus
across two phases, housing 450 companies which employ more than 50,000 professionals as of 2020.
Infopark is 10 kilometres (6 mi) from downtown Kochi and 22 kilometres (14 mi) from the Cochin
International Airport. 

In addition to conference hall in the Athulya building in InfoPark, two big conference halls and small rooms from
Hotel Four Points by Sheraton Kochi Infopark (300 meters from Athulya building) will be used for talks,
BoFs and hacklabs. [Accommodation](../accommodation/) is also arranged in the same hotel.

<br />
## Codes of Conduct and Community Team

Please see the [Code of Conduct](../coc/) page for more information.

We look forward to seeing you in Kochi!
